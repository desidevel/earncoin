import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';
import { Http } from '@angular/http'
import { User } from '../../providers';
import { MainPage } from '../';

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {
  // The account fields for the login form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  // account: { username: string, password: string, email: string } = {
  //   username: '',
  //   email: '',
  //   password: ''
  // };
     username: any;
    email: any;
    password: any;

  // Our translated text strings
  private signupErrorString: string;

  constructor(public navCtrl: NavController,
    public user: User,
    public toastCtrl: ToastController,
    public translateService: TranslateService, public http:Http) {

    this.translateService.get('SIGNUP_ERROR').subscribe((value) => {
      this.signupErrorString = value;
    })
  }

  // doSignup() {
  //   // Attempt to login in through our User service
  //   this.user.signup(this.account).subscribe((resp) => {
  //     this.navCtrl.push(MainPage);
  //   }, (err) => {

  //     this.navCtrl.push(MainPage);

  //     // Unable to sign up
  //     let toast = this.toastCtrl.create({
  //       message: this.signupErrorString,
  //       duration: 3000,
  //       position: 'top'
  //     });
  //     toast.present();
  //   });
  // }
  doSignup(){
    console.log("SIGN UP INITITAED")
    let data = {
      username: this.username,
      password: this.password,
      email: this.email
    }
    return new Promise((resolve, reject) => {
      this.http.post("http://serverside.pythonanywhere.com/register/", (data))
        .subscribe(res => {
          
          this.navCtrl.push(MainPage);
          resolve(res);
        }, (err) => {
          reject(err);
          let toast = this.toastCtrl.create({
                  message: this.signupErrorString,
                  duration: 3000,
                  position: 'top'
                });
                toast.present();
              });
        });
}
}

