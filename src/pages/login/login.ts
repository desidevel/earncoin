import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';
import { User } from '../../providers';
import { MainPage } from '../';
import { Http } from '@angular/http'


import { Storage } from '@ionic/storage';
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  // The account fields for the login form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  account: { email: string, password: string } = {
    email: 'test@example.com',
    password: 'test'
  };
  userInfo:any;

  username:any;
  password:any;
  // Our translated text strings
  private loginErrorString: string;

  constructor(public navCtrl: NavController,
    public user: User,
    private storage: Storage,
    public http: Http,

    public toastCtrl: ToastController,
    public translateService: TranslateService) {

    this.translateService.get('LOGIN_ERROR').subscribe((value) => {
      this.loginErrorString = value;
    })
  }

  // Attempt to login in through our User service
  // doLogin() {
  //   this.user.login(this.account).subscribe((resp) => {
  //     this.navCtrl.push(MainPage);
  //   }, (err) => {
  //     this.navCtrl.push(MainPage);
  //     // Unable to log in
  //     let toast = this.toastCtrl.create({
  //       message: this.loginErrorString,
  //       duration: 3000,
  //       position: 'top'
  //     });
  //     toast.present();
  //   })
  //   ;
  // }
  doLogin(){
    console.log("SIGN UP INITITAED")
    let data = {
      username: this.username,
      password: this.password,
      
    }
    return new Promise((resolve, reject) => {
      this.http.post("http://serverside.pythonanywhere.com/login/", (data))
        .subscribe(res => {
          console.log(res)
          this.navCtrl.push(MainPage);
          resolve(res);
          this.userInfo = res
          this.myLocalStorage(this.userInfo._body)
          console.log(this.userInfo._body)
          
        }, (err) => {
         
          let toast = this.toastCtrl.create({
                  message: 'Login failed, check your username & password',
                  duration: 3000,
                  position: 'bottom'
                });
                toast.present();
              });
        });
}

  myLocalStorage(username){
    this.storage.set('userInfo', username);


  }

}
